
<?php

    class Room {
        public int $_roomNumber;
        public int $_price;
        public bool $_isReserved;
        public int $_nbBeds;
        public bool $_isWifi;
        public Hotel $_hotel;
        public array $_reservations = [];

        public function __construct(int $roomNumber, int $price, bool $isReserved, int $nbBeds, 
        bool $isWifi, Hotel $hotel)
        {
            $this->_roomNumber = $roomNumber;
            $this->_price = $price;
            $this->_isReserved = $isReserved;
            $this->_nbBeds = $nbBeds;
            $this->_isWifi = $isWifi;
            $this->_hotel = $hotel;
            $this->_reservations = [];
            $this->_hotel->addRoom($this);
        }

        public function getRoomNumber(): int
        {
                return $this->_roomNumber;
        }

        public function setRoomNumber(int $_roomNumber): self
        {
                $this->_roomNumber = $_roomNumber;

                return $this;
        }

        public function getPrice(): int
        {
                return $this->_price;
        }

        public function setPrice(int $_price): self
        {
                $this->_price = $_price;

                return $this;
        }

        public function getIsReserved(): bool
        {
                return $this->_isReserved;
        }

        public function setIsReserved(bool $_isReserved): self
        {
                $this->_isReserved = $_isReserved;

                return $this;
        }

        public function getNbBeds(): int
        {
                return $this->_nbBeds;
        }

        public function setNbBeds(int $_nbBeds): self
        {
                $this->_nbBeds = $_nbBeds;

                return $this;
        }

        public function getIsWifi(): bool
        {
                return $this->_isWifi;
        }

        public function setIsWifi(bool $_isWifi): self
        {
                $this->_isWifi = $_isWifi;

                return $this;
        }

        
        public function getHotel(): Hotel
        {
                return $this->_hotel;
        }

        public function setHotel(Hotel $_hotel): self
        {
                $this->_hotel = $_hotel;

                return $this;
        }

        

        public function __toString()
        {
                return "Room number : " . $this->_roomNumber . "<br>" . "Price : " . 
                $this->_price . "<br>" . "Number of beds : " . $this->_nbBeds ."<br>" ;
        }



        public function addReservation(Reservation $reservation): void
        {
            array_push($this->_reservations, $reservation);
            $this->_isReserved = true;
        }

        

    }
        





?>