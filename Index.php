<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Exercice 2 : The Hotel</title>
</head>
<body>

<h1>Exercice 2 : The Hotel</h1>


<?php

spl_autoload_register(function ($class_name) {
    require $class_name . '.php';
});

//$hotel = new Hotel("name","city",address,stars,nbRooms)    
$helton = new Hotel("Helton","Strasbourg","Rue du vieux marché aux vins",4, 10);
$regent = new Hotel("Regent","Paris","Rue des hotels",4, 25);

//room = new Room(roomNumber,price,isReserved,nbBeds,isWifi,client,hotel)
$room1 = new Room(1, 1200, false, 2, true, $helton);
$room2 = new Room(2, 1200, false, 2, true, $helton);
$room3 = new Room(3, 1200, false, 2, true, $helton);
$room4 = new Room(4, 1300, false, 3, true, $helton);
$room5 = new Room(5, 1300, false, 3, true, $helton);
$room6 = new Room(6, 1300, false, 3, true, $helton);
$room7 = new Room(17, 1300, false,3, true, $helton);
$room8 = new Room(18, 1400, false, 4, true, $helton);
$room9 = new Room(19, 1500, false, 4, true, $helton);
$room10 = new Room(20, 1600, false,4, true, $helton);
//client = new Client("name","firstname")
$mike = new Client("Mika","Murmann");
$virgile = new Client("Virgile","Truc");



//reservation = new Reservation(hotel,room,client,dateDebut,dateFin)

$reservation1 = new Reservation($helton, $room3, $mike, new DateTime("2021-03-11"), new DateTime("2021-03-15"));
$reservation2 = new Reservation($helton, $room4, $mike, new DateTime("2021-04-01"), new DateTime("2021-04-17"));
$reservation3 = new Reservation($helton, $room7, $virgile, new DateTime("2021-05-01"), new DateTime("2021-06-17"));

echo $helton->displayInfo();
echo $helton->displayReservation();
echo $regent->displayReservation();

echo $mike->displayReservation();

echo $helton->roomStatus();




?>
</body>
</html>