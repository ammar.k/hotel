<?php

    class Reservation{
        private Hotel $_hotel;
        private Room $_room;
        private Client $_client;
        private DateTime $_dateDebut;
        private DateTime $_dateFin;

        public function __construct(Hotel $hotel, Room $room, Client $client, DateTime $dateDebut, Datetime $dateFin)
        {
            $this->_hotel = $hotel;
            $this->_room = $room;
            $this->_client = $client;
            $this->_dateDebut = $dateDebut;
            $this->_dateFin = $dateFin;
            $this->_client->addReservation($this);
            $this->_room->addReservation($this);
            $this->_room->setIsReserved(true);
            $this->_hotel->addReservation($this);
        }

        public function getChambre(): Room
        {
                return $this->_room;
        }

        public function setChambre(Room $_chambre): self
        {
                $this->_room = $_chambre;

                return $this;
        }

        public function getClient(): Client
        {
                return $this->_client;
        }

        public function setClient(Client $_client): self
        {
                $this->_client = $_client;

                return $this;
        }

        public function getDateDebut(): DateTime
        {
                return $this->_dateDebut;
        }

        public function setDateDebut(DateTime $_dateDebut): self
        {
                $this->_dateDebut = $_dateDebut;

                return $this;
        }

        public function getDateFin(): DateTime
        {
                return $this->_dateFin;
        }

        public function setDateFin(DateTime $_dateFin): self
        {
                $this->_dateFin = $_dateFin;

                return $this;
        }
        public function addReservation(Reservation $reservation): void
        {
            array_push($this->_client->getReservations(), $reservation);
            
        }

        

        public function __toString()
        {
            return  $this->_client . " Room " . $this->_room->getRoomNumber() . " from " .
            $this->_dateDebut->format('Y-m-d') . " to " . $this->_dateFin->format('Y-m-d');
        }



    }



?>