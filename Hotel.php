<?php

class Hotel
{
        public string $_name;
        public string $_city;
        public string $_address;
        public int $_stars;
        public int $_nbRooms;
        public array $_rooms = [];
        public array $_reservations = [];
        


        public function __construct(string $name, string $city,string $address, int $stars, int $nbRooms)
        {
            $this->_name = $name;
            $this->_city = $city;
            $this->_address = $address;
            $this->_stars = $stars;
            $this->_nbRooms = $nbRooms;
            $this->_rooms = [];
            $this->_reservations = [];
            
        }

 
        public function getName(): string
        {
                return $this->_name;
        }

        public function setName(string $_name): self
        {
                $this->_name = $_name;

                return $this;
        }


        public function getCity(): string
        {
                return $this->_city;
        }

 
        public function setCity(string $_city): self
        {
                $this->_city = $_city;

                return $this;
        }


        public function getStars(): int
        {
                return $this->_stars;
        }

  
        public function setStars(int $_stars): self
        {
                $this->_stars = $_stars;

                return $this;
        }

        public function getNbRooms(): int
        {
                return $this->_nbRooms;
        }

        public function setNbRooms(int $_nbRooms): self
        {
                $this->_nbRooms = $_nbRooms;

                return $this;
        }


        public function getAdress(): string
        {
                return $this->_address;
        }

        public function setAdress(string $_adress): self
        {
                $this->_address = $_adress;

                return $this;
        }

        public function convertToStars(): string
        {
                $stars = "";
                for ($i = 0; $i < $this->_stars; $i++) {
                        $stars .= "*";
                }
                return $stars;
        }

        public function getRooms(): array
        {
                return $this->_rooms;
        }

        public function getResevations(): array
        {
                return $this->_reservations;
        }

        public function addRoom(Room $room): void
        {
                array_push($this->_rooms, $room);
        }

        public function __toString(): string
        {
                return  $this->_name . " " . $this->convertToStars() . " " . $this->_city ;
        }

        public function displayInfo(){

                $nbReserved = count($this->_reservations);
                $nbFreeRoom = $this->_nbRooms - $nbReserved;

                $results = "<h3> $this  </h3><p> ".
                 "Address : " . $this->_address . "<br>".
                "Number of rooms : " . $this->_nbRooms . "<br>".
                "Number of reserved rooms :".$nbReserved. "<br>".
                "Number of free rooms :".$nbFreeRoom. "</p>";

                return $results;

        }

        public function addReservation(Reservation $reservation): void
        {
                array_push($this->_reservations, $reservation);
                //array_push($this->_rooms, $reservation->getChambre());
        }

        public function displayReservation(){

                $totalReservations = count($this->getResevations());
                if($totalReservations == 0){
                        $results = "<h3>Reservation of the $this hotel</h3>" . "<p style=color:red;> No reservation </p>";
                }
                else{
                $results = "<h3> Reservations of   $this  </h3>";
                $results .=  "<p style=color:green;> Reservation $totalReservations </p>";
                foreach ($this->getResevations() as $reservation) {
                        $results .= $reservation . "<br>";
                }
                }
                return $results;
        }

        public function roomStatus(){
                $result = "<h3> Rooms of the $this hotel </h3>";
                $result .= "<table border='1 solid black;' style='border-collapse: collapse; 
                width: 50%; text-align: center;'>";
                $result.= "<thead>";
                $result.= "<tr>";
                $result.= "<th>Room</th>";
                $result.= "<th>Price</th>";
                $result.= "<th>Wifi</th>";
                $result.= "<th>State</th>";
                $result.= "</tr>";
                $result.= "</thead>";
                $result.= "<tbody>";
                foreach ($this->_rooms as $room) {
                        $result.= "<tr>";
                        $result.= "<td>" . $room->getRoomNumber() . "</td>";
                        $result.= "<td>" . $room->getPrice() . "</td>";
                        if($room->getIsWifi() == true){
                                
                        $result.= "<td style=color:green;>" ."Yes" . $room->getIsWifi() . "</td>";
                        }else{
                                $result.= "<td style=color:red;>" ."No" . $room->getIsWifi() . "</td>";
                        }
                        if($room->getIsReserved() == true){
                                $result.= "<td style=color:red;>" ."reserved" . $room->getIsReserved() . "</td>";
                        }else{
                                $result.= "<td style=color:green;>" ."free" . $room->getIsReserved() . "</td>";
                        }
                        $result.= "</tr>";
                }

                return $result;


        }



        
}




?>