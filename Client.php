
<?php

    class Client{
        public string $_name;
        public string $_surname;
        private array $_reservations = [];

        public function __construct(string $name, string $surname)
        {
            $this->_name = $name;
            $this->_surname = $surname;
            $this->_reservations = [];

        }

        public function getName(): string
        {
                return $this->_name;
        }

        public function setName(string $_name): self
        {
                $this->_name = $_name;

                return $this;
        }

        public function getSurname(): string
        {
                return $this->_surname;
        }

        public function setSurname(string $_surname): self
        {
                $this->_surname = $_surname;

                return $this;
        }

        public function getReservations(): array
        {
                return $this->_reservations;
        }

        public function __toString()
        {
            return " " . $this->_name . " " . $this->_surname;
        }

        public function addReservation(Reservation $reservation): void
        {
            array_push($this->_reservations, $reservation);
        }

        public function displayReservation(){
            $nbReservations = count($this->getReservations());
            $results = "<h4> Reservations of " . $this . " : </h4>";
            if($nbReservations == 0){
                $results .= "<p style=color:red;> $nbReservations Reservations </p>";
            }else {
            $results .= "<p style=color:green;> $nbReservations Reservations </p>";
            foreach ($this->getReservations() as $reservation) {
                $results .= $reservation . "<br>";
            }
        }
            return $results;
        }

    }



?>